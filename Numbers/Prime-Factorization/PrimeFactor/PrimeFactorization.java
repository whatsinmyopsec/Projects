package PrimeFactor;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class PrimeFactorization {
    public static List<Double> primeFactors(double number) {
        double n = number;
        List<Double> factors = new ArrayList<>();
        for (double i = 2; i <= n; i++) {
            while (n % i == 0) {
                factors.add(i);
                n /= i;
            }
        }
        return factors;
    }

    public static void main(String[] args) {
        System.out.println("Enter a number for prime factorization");

        Scanner scanner = new Scanner(System.in);
        final double x = Double.parseDouble(scanner.nextLine());
        primeFactors(x).forEach(System.out::println);
        scanner.close();
    }
}
