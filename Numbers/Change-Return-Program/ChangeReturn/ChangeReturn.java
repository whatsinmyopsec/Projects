package ChangeReturn;

import java.util.Scanner;

public class ChangeReturn {
    public static void main(String[] args) {

        Scanner keyboard = new Scanner(System.in);
        System.out.print("\nEnter the total amount of the item: ");
        double totalAmt = keyboard.nextDouble();
        System.out.print("Enter the amount given to the cashier: ");
        double amtGiven = keyboard.nextDouble();

        // define variables for coins
        double two = 2.00;
        double one = 1.00;
        double fiftyCent = 0.50;
        double twentyCent = 0.20;
        double tenCent = 0.10;
        double fiveCent = 0.05;

        // round changeDue to 2 decimal places and calculate the modulus in a hierarchy
        double changeDue = ((double) ((int) Math.round((amtGiven - totalAmt) * 100)) / 100.0);
        double modTwo = ((double) ((int) Math.round((changeDue % two) * 100)) / 100.0);
        double modOne = ((double) ((int) Math.round((modTwo % one) * 100)) / 100.0);
        double modFiftyCent = ((double) ((int) Math.round((modOne % fiftyCent) * 100)) / 100.0);
        double modTwentyCent = ((double) ((int) Math.round((modFiftyCent % twentyCent) * 100)) / 100.0);
        double modTenCent = ((double) ((int) Math.round((modTwentyCent % tenCent) * 100)) / 100.0);
        double modFiveCent = ((double) ((int) Math.round((modTenCent % fiveCent) * 100)) / 100.0);

        // count number of coins
        int numTwo = (int) ((changeDue - modTwo) / (two));
        int numOne = (int) ((modTwo - modOne) / (one));
        int numFiftyCent = (int) ((modOne - modFiftyCent) / (fiftyCent));
        int numTwentyCent = (int) ((modFiftyCent - modTwentyCent) / (twentyCent));
        int numTenCent = (int) ((modTwentyCent - modTenCent) / (tenCent));
        int numFiveCent = (int) ((modTwentyCent - modFiveCent) / (fiveCent));

        // return information to user
        System.out.println("\nTotal amount of change to give: " + changeDue);
        System.out.println("Number of two's to give: " + numTwo);
        System.out.println("Number of one's to give: " + numOne);
        System.out.println("Number of fifty cents to give: " + numFiftyCent);
        System.out.println("Number of twenty cents to give: " + numTwentyCent);
        System.out.println("Number of ten cents to give: " + numTenCent);
        System.out.println("Number of five cents to give: " + numFiveCent);
    }
}