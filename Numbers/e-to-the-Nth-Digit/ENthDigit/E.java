package ENthDigit;

import java.util.Scanner;
import java.io.IOException;
import java.util.InputMismatchException;

public class E {
    public static void main(String[] args) throws IOException {
        final double E = Math.E;
        double eToNthDigit;
        Scanner input = new Scanner(System.in);
        int n;


        try {
            n = Integer.parseInt(args[0]); //digit limit

        } catch (NumberFormatException e) {
            System.out.println("NumberFormatException: The argument given is not an Integer.");
            return;

        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.print("Please provide an Integer as an argument for the program: ");
            try {
                n = input.nextInt();

            } catch (InputMismatchException ex) {
                System.out.println("InputMismatchException: The argument given is not an Integer.");
                return;
            }
        }

        if (n < 0) {
            System.out.println("ERROR: The number given is a negative number");
        } else {
            eToNthDigit = (Math.floor(E * Math.pow(10, n))) / Math.pow(10, n);
            System.out.println("e with " + n + " decimal places is: " + eToNthDigit);
        }


    }
}