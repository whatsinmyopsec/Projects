package Mortgage;

import java.util.Scanner;

/**
 * Calculate the monthly payments of a fixed term mortgage over given Nth terms
 * at a given interest rate.
 */

public class Mortgage {

    public static void main(String[] args) {

        Scanner keyboard = new Scanner(System.in);
        double loan;
        double interestRate;
        double monthlyPayment;
        double balance;
        int term;

        System.out.printf("Enter the loan amount: ");
        loan = keyboard.nextDouble();

        System.out.printf("Enter the interest rate on the loan: ");
        interestRate = keyboard.nextDouble();

        System.out.printf("Enter the term(years) for the loan payment: ");
        term = keyboard.nextInt();

        System.out.printf("\n================================================================\n");
        keyboard.close();

        monthlyPayment = getMonthlyPayment(loan, interestRate, term);
        balance = -(monthlyPayment * (term * 12));
        System.out.format("%-30s$%-+10.2f%n", "Amount owed to bank:", balance);
        System.out.format("%-30s$%-10.2f%n", "Minimum monthly payment:", monthlyPayment);
    }

    public static double getMonthlyPayment(double loan, double interestRate, double term) {
        double rate = (interestRate / 100) / 12;
        double base = (rate + 1);
        double months = term * 12;
        double result;
        result = loan * (rate * (Math.pow(base, months)) / ((Math.pow(base, months)) - 1));

        return result;
    }
}